"""
"""

TINYMCE_PLUGINS = [
	'safari',
	'table',
	'advlink',
	'advimage',
	'iespell',
	'inlinepopups',
	'media',
	'searchreplace',
	'contextmenu',
	'paste',
	'wordcount'
]

TINYMCE_DEFAULT_CONFIG = {
    'theme' : "advanced",
    'plugins':','.join(TINYMCE_PLUGINS),
    'theme_advanced_buttons1' : "formatselect,italic,bold,underline,|,\
				bullist,numlist,|,\
				justifyleft,justifycenter, justifyright,justifyfull,|,\
				undo,|,link,unlink,|,\
				table,delete_row,delete_table,row_after,row_before,|,\
				outdent,indent",
				
    'theme_advanced_buttons2':"image,anchor,code,|,hr,removeformat,sub,sup,charmap,visualaid,forecolor,backcolor",
    'theme_advanced_buttons3' : "",
    'theme_advanced_blockformats' : "div,h1,h2,h3,h4,h5,h6",
    #'theme_advanced_containers' : "theme_advanced_blockformats",
    'theme_advanced_toolbar_location' : "top",
    'theme_advanced_toolbar_align' : "left",
    'theme_advanced_statusbar_location' : "bottom",
    'theme_advanced_resizing' : True,
    'theme_advanced_cleanup':False
}
