__author__ = 'john'

from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView

from models import Work

urlpatterns = patterns('',
    url(r'^$', ListView.as_view(queryset=Work.objects.filter(published=True)),
        name='work-list'),

    url(r'^(?P<slug>[\w-]+)/$', DetailView.as_view(queryset=Work.objects.filter(published=True)),
        name='work-detail'),

)

