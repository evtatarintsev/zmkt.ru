# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse

from tinymce.models import HTMLField


class Work(models.Model):
    title = models.CharField(max_length=80, verbose_name=u'Заголовок')
    slug = models.SlugField()
    text = HTMLField(verbose_name=u'Текст')
    image = models.ImageField(upload_to='works', null=True, blank=True)
    client = models.CharField(max_length=80, blank=True)
    info = models.CharField(max_length=80, blank=True)
    link = models.CharField(max_length=80, blank=True)
    date = models.DateField()
    published = models.BooleanField(default=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("work-detail", kwargs={'slug':self.slug})
