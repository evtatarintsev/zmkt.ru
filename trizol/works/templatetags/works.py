# -*- coding: utf-8 -*-
__author__ = 'john'

from django import template

from trizol.works.models import Work

register = template.Library()


@register.assignment_tag()
def get_last_works(count=4):
    return Work.objects.filter(published=True)[:count]
