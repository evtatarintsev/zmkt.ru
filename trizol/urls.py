from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
admin.autodiscover()

from trizol.views import HomeView

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^service/', include('trizol.service.urls')),
    url(r'^works/', include('trizol.works.urls')),
    url(r'^news/', include('trizol.news.urls')),
    url(r'^about/$',TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^contacts/$',TemplateView.as_view(template_name='contacts.html'), name='contacts'),
    url(r'^admin/', include(admin.site.urls)),
)



if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
) + urlpatterns