__author__ = 'john'

from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView

from models import Entry

urlpatterns = patterns('',
    url(r'^$', ListView.as_view(queryset=Entry.objects.filter(published=True)),
        name='entry-list'),

    url(r'^(?P<slug>[\w-]+)/$', DetailView.as_view(queryset=Entry.objects.filter(published=True)),
        name='entry-detail'),

)

