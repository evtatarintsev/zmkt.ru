# -*- coding: utf-8 -*-
__author__ = 'john'

from django import template

from trizol.news.models import Entry

register = template.Library()


@register.assignment_tag()
def get_last_news(count=3):
    return Entry.objects.filter(published=True)[:count]