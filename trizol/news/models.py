# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse

from tinymce.models import HTMLField


class Entry(models.Model):
    title = models.CharField(max_length=80)
    slug = models.SlugField()
    description = models.TextField()
    text = HTMLField()
    image = models.ImageField(upload_to='news')
    published = models.BooleanField(default=True)
    date_added = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ['-date_added', ]

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("entry-detail", kwargs={'slug':self.slug})