__author__ = 'john'

from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView

from models import Service

urlpatterns = patterns('',
    url(r'^$', ListView.as_view(queryset=Service.objects.filter(published=True)),
        name='service-list'),

    url(r'^(?P<slug>[\w-]+)/$', DetailView.as_view(queryset=Service.objects.filter(published=True)),
        name='service-detail'),

)

