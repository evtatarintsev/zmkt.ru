# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse

from tinymce.models import HTMLField


class Service(models.Model):
    title = models.CharField(max_length=80, verbose_name=u'Заголовок')
    slug = models.SlugField()
    description = models.TextField(verbose_name=u'Описание')
    text = HTMLField(verbose_name=u'Текст')
    image = models.ImageField(upload_to='service', null=True, blank=True)
    published = models.BooleanField(default=True)
    primary = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("service-detail", kwargs={'slug':self.slug})