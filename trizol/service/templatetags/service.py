# -*- coding: utf-8 -*-
__author__ = 'john'

from django import template

from trizol.service.models import Service

register = template.Library()


@register.assignment_tag()
def get_primary_service():
    return Service.objects.filter(published=True, primary=True)

@register.assignment_tag()
def get_all_service():
    return Service.objects.filter(published=True)