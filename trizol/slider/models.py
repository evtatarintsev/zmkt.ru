# -*- coding: utf-8 -*-
from django.db import models


class Slide(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'Название')
    text = models.TextField()
    image = models.ImageField(upload_to='slider')
    published = models.BooleanField(default=True)
    position = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ['position',]

    def __unicode__(self):
        return self.name