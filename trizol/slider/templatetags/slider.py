__author__ = 'john'

from django import template

from trizol.slider.models import Slide

register = template.Library()


@register.assignment_tag()
def get_slider():
    return Slide.objects.filter(published=True)
